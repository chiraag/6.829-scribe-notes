\documentclass{article}
\usepackage[T1]{fontenc}
\usepackage{amssymb, amsmath, graphicx}
\usepackage{mathtools}

\setlength{\oddsidemargin}{.25in}
\setlength{\evensidemargin}{.25in}
\setlength{\textwidth}{6in}
\setlength{\topmargin}{-0.4in}
\setlength{\textheight}{8.5in}

\newcommand{\heading}[6]{
  \renewcommand{\thepage}{#1-\arabic{page}}
  \noindent
  \begin{center}
  \framebox{
    \vbox{
      \hbox to 5.78in { \textbf{#2} \hfill #3 }
      \vspace{4mm}
      \hbox to 5.78in { {\Large \hfill #6  \hfill} }
      \vspace{2mm}
      \hbox to 5.78in { \textit{Instructor: #4 \hfill #5} }
    }
  }
  \end{center}
  \vspace*{4mm}
}

\newtheorem{theorem}{Theorem}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{observation}[theorem]{Observation}
\newtheorem{fact}[theorem]{Fact}
\newtheorem{assumption}[theorem]{Assumption}

\newenvironment{proof}{\noindent{\bf Proof:} \hspace*{1mm}}{
	\hspace*{\fill} $\Box$ }
\newenvironment{proof_of}[1]{\noindent {\bf Proof of #1:}
	\hspace*{1mm}}{\hspace*{\fill} $\Box$ }
\newenvironment{proof_claim}{\begin{quotation} \noindent}{
	\hspace*{\fill} $\diamond$ \end{quotation}}

\newcommand{\lecturenotes}[4]{\heading{#1}{6.829: Computer Networks}{#2}{Hari Balakrishnan}{#3}{#4}}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE MODIFY THESE FIELDS AS APPROPRIATE
\newcommand{\lecturenum}{6}          % lecture number
\newcommand{\lecturetitle}{Lecture 16: Flat Overlays and DHTs [Chord]} % topic of lecture
\newcommand{\lecturedate}{April 8, 2013}  % date of lecture
\newcommand{\studentname}{Xinyue Ye, Chen Zhao}    % full name of student (i.e., you)
% PUT HERE ANY PACKAGES, MACROS, etc., ADDED BY YOU
% ...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\lecturenotes{\lecturenum}{\lecturedate}{\studentname}{\lecturetitle}


\section{Peer-To-Peer Systems}
Peer-to-peer (P2P) systems research has increased substantially in the last few years.  It is much more symmetric than the client-server model.  In P2P, each user can be viewed as both a client and a server.
\begin{itemize}
\item
In 2008, P2P accounts for 54.46\% of all internet traffic in south-west Europe. 
\item 
In 2012, Over 152 million use BitTorrent regularly.  
\end{itemize}
\begin{center}
\includegraphics[scale=0.4]{p2p-networks.jpg}
\end{center}
P2P networks offer several advantages over client-server networks:
\begin{enumerate}
\item
In P2P, resources and content are shared by all peers, in contrast to client-server, where the server is responsible for sharing resources.  In addition, no system administrator is needed.
\item
P2P is more reliable than client-server model as it eliminates dependency on a central server.  
\item
P2P is resilient to geographical failure in one location, or legal failure.
\item
The cost to maintain a P2P network is considerably less.
\end{enumerate}
These characteristics make the peer-to-peer network more robust and scalable than a centralized network.

\section{Chord}
Chord is a distributed lookup protocol designed to efficiently locate the peer that stores a particular item in a P2P network.  It uses a distributed hash table (DHT) that maps a key to a node or peer, which is responsible for the value of that key.  Given a key, Chord will locate the node that stores that key.  Some nodes may be responsible for up to $\log n$ keys.  

\subsection*{Balanced Load}
One naive way of distributing keys is to divide them equally among all the nodes.  Thus, node $n_1$ is responsible for the fraction of keys 0 to $1/N$, where $N$ is the number of keys. Node $n_2$ is responsible for the keys in the range $1/N$ to $2/N$, etc.    
\begin{align*}
0  \xrightarrow{n_1} \frac{1}{N} \xrightarrow{n_2} \frac{2}{N} \cdots
\frac{N-1}{N} \xrightarrow{n_N} 1 
\end{align*}
When a new node joins, keys need to be redistributed to keep the load balanced:
\begin{align*}
0  \xrightarrow{n_1} \frac{1}{N+1} \xrightarrow{n_2} \frac{2}{N+1} \cdots \frac{N}{N+1} \xrightarrow{n_{N+1}} 1 
\end{align*}
The fraction of keys $n_1$ needs to send to $n_2$ is $\frac{1}{N} - \frac{1}{N+1} = \frac{1}{N(N+1)}$.  $n_2$ in turns must send $\frac{2}{N(N+1)}$.  Thus, the total fraction of keys that needs to be moved is
\begin{align*}
\displaystyle \sum_{i=0}^{N} \frac{i}{N(N+1)} = \frac{1}{2}
\end{align*} 

\subsection*{Consistent Hashing}
Chord's approach is to use consistent hashing, which is to assign keys to nodes immediately after, called the \textit{successor} node, as show in the following figure.  

\begin{center}
\includegraphics[scale=0.6]{figure2.png}
\end{center}

Chord uses virtual nodes and random hash function SHA-1 to assign each node an $m$-bit identifier.  Each node has a finger table where the $i-1$th finger entry in node $n$'s table is the first node that is greater or equal to $n+2^{i-1}$. 

Consistent hashing allows nodes to join and leave the network with minimal disruption.  To maintain the consistent hashing mapping when a node $n$ joins the network, certain keys previously assigned to $n$'s successor now become assigned to $n$.  When node $n$ leaves the network, all of its assigned keys are reassigned to $n$'s successor.  No other changes in the assignment of keys to nodes is necessary.

\begin{theorem}
For any set of $N$ nodes and $K$ keys, with high probability:
\begin{enumerate}
\item
Each node is responsible for at most $(1+\log N)K/N$ keys
\item
When an $(N+1)^{st}$ node joins or leaves the network, responsibility for $O(K/N)$ keys changes hands (and only to or from the joining or leaving node).
\end{enumerate}
\end{theorem}
Here, the phrase "with high probability" means that the nodes and keys are randomly chosen so that it is unlikely to produce an unbalanced distribution in a non-adversarial model of the world.

\subsection*{Correctness}
Chord will never overshoot the node that has the key of interest, that is, it will always locate a node before where the key is located.  Correctness of Chord relies on the correctness of successors.  See figure.

\begin{center}
\includegraphics[scale=0.6]{figure3.png}
\end{center}

For resilience each node will keep $R$ successors.  Consider if each node fails with probability 1/2, then the probability that all successors of a node $n$ fail is $1/2^R$.  Thus, the probability that all successors fail over all $N$ nodes is no more than $N/2^R$ by the union bound.  
If we set $R = \log _2 N + 20$, then the probability that the whole network fails is no more than $N/(2^{\log _2N} \cdot 2^{20}) = 1/2^{20}$.  

\section{Examples of Peer-to-Peer Networks}
\subsection*{Napster}
Napster is a peer-to-peer music sharing service initiated in 1999.  In this network, a central database contains information on the location of each file.  When a node requests a file, it queries the central database, which responds with the file's location.  The node then retrieves the file from the node where it is stored.  Hence, the central database itself does not contain any file, only metadata on file locations.  If the central database fails, the network still fails.

\subsection*{Gnutella}
Gnutella is a local peer-to-peer network, and is one of the first decentralized P2P networks.  At initiator, Gnutella makes a unique ID for each message and flood to all neighbors of the node.  When a node receives a message, if it has already seen the message ID, then it drops the message. Otherwise, if it has the answer to the query, then it responds to the original sender.  If it does not have the answer, then it floods the message to its neighbors and decreases a time-to-live TTL counter by 1. When a message's TTL becomes 0, then nodes will drop the message when they receive it.   

This is called the gossip algorithm and networks that use this don't scale well on a global scope, so nodes should only flood to its local neighbors because the number of messages is on the order of $O(|E|)$ where $E$ is the set of edges/links.


\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
