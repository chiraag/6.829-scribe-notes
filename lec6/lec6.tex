\documentclass{6046}
\usepackage{wrapfig}
\author{Pavel Panchekha, Yonatan Belinkov}
\problemset{2}

\begin{document}
\section{Wireless Networks (Introduction)}

\title{Lecture 6 - Wireless Networks}

So far in the course we have looked at resource management strategies,
where the fundamental problem was sharing at the switch. In wireless
networks, interactions between different connections appear in
different layers, including the physical layer. As in other networking
domains, the main problems in wireless networks are sharing,
reliability, and scalability.

How is wireless different? Radios are different because:

\begin{enumerate}
\item Sharing is an end-point problem.  In wired links, sharing is a
  problem that shows up at the switch.  Congestion and the like do
  not concern the end-points too much, because each of them has a
  dedicated line to the switch.  In wireless communication, however,
  the medium by which end-points talk to the switch is shared, so
  end-points have to be concerned with sharing as well.
  
\item Link rates vary. While all media theoretically have multiple
  bit rates and allow you to choose, we can usually pick one bit
  rate in wired networks and stick to it.  With wireless we cannot
  do that, because channel conditions vary with time -- they depend,
  for example, on the geometry of the communication, and laptops and
  other devices can move around.
  
\item Shared medium. Sharing the medium with multiple nodes means
  that we have to deal with interference.
\end{enumerate}

\section{Properties of wireless reception}

It makes sense to start off the discussion of wireless networks by
noting some properties peculiar to wireless networks.

\begin{enumerate}
\item Probabilistic packet reception. This is a property of the link
  itself (which is rare in a wired medium).
\item This probability changes with time. This means that bit-rate
  selection should maximize throughput, not minimize packet loss. In
  fact, sending at high bit-rates necessarily means losing packets,
  because if we are not losing packets, then we are probably not fully
  utilizing the link.
\item Broadcast receptions and spatial diversity. Multiple receivers
  might receive the packet. This allows for designing multiple
  antennas where receptions are independent, and combining multiple
  receptions to account for errors and noise.
 \end{enumerate}

These properties present both challenges and opportunities. For
example, property (1) means that we can, and should transmit in
varying bit-rates. Property (2) is mostly a challenge, but also allows
for employing a scheduling algorithm that times packet with especially
good channel conditions (Proportionally Fair Scheduling). Property (3)
presents a challenge in figuring out which base stations to
communicate with, but it also means that different antennas can
cooperate.

\section{Topics to be covered}

We intend to cover a few topics in wireless networks.

\begin{enumerate}
\item Designing good point-2-point communication links, including
  bit-rate selection/adaptation and rateless communication.
\item Sharing a medium.  MAC protocols (like CSMA), cell phone
  scheduling algorithms (as in 3G/LTE), handling interference (such as
  Zig-zag coding), and constructing networks with multiple antennas.
\item Sharing spectrum and spectrum policy.  A focus on white spaces,
  discussing priorities in spectrum use and under-utilized spectrum.
\item Mesh networks, also called multi-hub networking.  This is not
  the same as something like cellular transmissions, because here
  there are multiple, possibly many, wireless hops between the source
  and destination.
\item Mobility, since wireless networks encourage devices that can
  move between networks.  Mobility can be solved independent of the
  medium; wireless simply makes things more pressing.
 \end{enumerate}

\section{Bit-rate adaptation}

The first thing to talk about is bit-rate adaptation.  As mentioned,
choosing a correct bit rate is a balance between throughput and
error.  At too low a bit rate, you're simply not using all of the
throughput in a channel, so communication will be unnecessarily slow.
If your bit rate is too high, the error rate will mean that the actual
rate at which packets arrive will be very low, due to retransmissions
and such.

To understand how bit-rate adaptation works, we first need to
understand how wireless networks send data.  Figure~\ref{fig:layering}
has a diagram of the traditional layering of a wireless protocol.  At
the bottom you have the physical layer, which concerns itself with
coding.  Above that, you have the link layer, which you might imagine
has two halves: one that worries about MAC, and one that worries about
framing and retransmission.  Above that, you have the subnetwork
layer, which does path selection -- which base station to talk to and
so on.  Finally, you have the normal network, IP, and TCP and so on
layers above that.

\begin{wrapfigure}{o}{.4\linewidth}
\begin{figure}[h]
 \begin{center}
  \includegraphics[width=.2\textwidth]{layering.png}
  \end{center}
 \caption{The layers of a network protocol}
 \label{fig:layering}
\end{figure}
\end{wrapfigure}

Of course, this is a ``traditional'' view of the layering model,
because in actuality we see a lot of benefits from cross-layer design,
especially in current research.  For example, Soft-Rate, which was a
reading, needing to pass extra information all the way up the stack to
tell the link layer which bits were reliable and which were not.
Zig-zag similarly needs cross-layer information.  So these layer
interfaces are wide, not narrow like in traditional wired Internet.
And unfortunately, usually all of these layers are bound together in a
single driver, so you can't just switch the link layer, or the
physical layer, like you can switch out TCP congestion control
algorithms.  It's like the bad old days of Ethernet, all over again!
Alas.  Some day we may have mostly software-controlled radios, and
then this splitting of layers may once again be a possibility.

Anyway, we now need to work from the bottom layer upwards.  The first
step is understanding modulation and coding -- this is where the
bit-rate selection happens.

\subsection{Modulation and Coding}

The general idea in coding is to take bits and turn them into radio
waves.  How do we do that?  Well, the most obvious answer is to only
send a wave for on bits and send no wave for off bits.  This is called
on-off key, and it is a plausible way of doing things.  But it is not
very efficient.  See, it's not too hard to measure the phase of a
wave, and that has a lot of possible values.  So we can send more
information per time slice by sending a wave with one of many offsets.
Similarly, we can send a wave with one of many intensities.

This forms the basis of the use of \textit{symbols}.  Before sending,
bits are converted into a sequence of symbols, that are then sent over
the wire one-by-one.  Symbols are drawn from a \textit{constellation},
a set of $2^k$ different pairs of phase and intensity, and each
represents $\log_2 2^k = k$ bits of information.  So we take blocks of
$k$ bits and convert each to a symbol, and then send that symbol (see
figure~\ref{fig:symbols}).

\begin{figure}[h]
 \begin{center}
  \includegraphics[width=.6\textwidth]{symbols.png}
  \end{center}
 \caption{Encoding messages into symbols}
 \label{fig:symbols}
\end{figure}

Lots of such schemes exist.  Of course, the on-off key can be
represented as a constellation: two symbols, one with intensity 0 and
one with intensity 1 and a given phase.  But better perhaps would be
two symbols with the same intensity and opposite phase -- these would
be easier to distinguish and thus be more noise-tolerant.  If we
wanted a higher bit rate, we could use a constellation that included
more symbols and thus more choices.  For example, QPSK has four
symbols, each of equal intensities and positioned as far away in phase
from each other as possible.  And nowadays there are codes like QAM-16
and QAM-64, which have 16 and 64 symbols, respectively.
Figure~\ref{fig:constellations} shows pictures of the intensity-phase
values of the symbols in various commonly-used constellations.

\begin{figure}
  \begin{center}
   \includegraphics[width=.5\textwidth]{constellations.png}
  \end{center}
  \caption{Some constellations used in wireless networking}
  \label{fig:constellations}
\end{figure}

Now, why would we ever want a constellation with fewer symbols?  Well,
in wireless communication we have to contend with noise, and you can
imagine noise as randomly bumping the symbols around.  If you have a
lot of noise and your symbols are close together in intensity and
phase, you can end up mistaking one symbol for another, which is a
failed packet (a checksum will fail to match), which causes
retransmissions.  This may be bad, so we may want to drop to a
constellation that yields a lower bit-depth.

By the way, you may wonder: how do we choose which bits map to which
symbol?  Well, we want nearby symbols to map to ``nearby'' bit strings
(in Hamming distance).  This way, if there is an error, we change only
a few bits, not all the bits, corresponding to that symbol.  For this,
we can use what's called a Gray code.

Note that after encoding to symbols, we need some kind of error
correction as well, so this adds some additional overhead to the
amount of data sent.  For example, you might want to send 100 bits,
but with error correction it is 120 bits.  And those 100 bits have an
IP header and a TCP header and so on.  And you might have to wait for
someone else to stop broadcasting per your MAC.  And there may be
errors that cause retransmission.  So you might have a 54 megabit per
second link that only really sends 30 megabits per second.  Or 20.

Now that we know how we can choose different bit rates, we need to
know why we want to choose a bit rate.  This brings us to a beautiful
result from information theory.

\subsection{Shannon Channel Theorem}

Claude Shannon, a mathematician of the mid-20th century, introduced
the notion of a channel's capacity.  His noise-channel theorem stated
that for a channel with signal-to-noise ratio $r$ and bandwidth $W$,
any bit rate up to
$$C = W \log_2 (1 + r)$$
is achievable with no errors, and higher rates are not achievable
without error.  The bandwidth $W$ here is measured in symbols per
second, and the $\beta = \log_2(1 + r)$ term is thus measured in bits
per symbol.  The reason this result is so beautiful is that you'd
think error-less communication was achievable only if you were OK with
very slow transmissions, doing lots of checking and retransmission.
But this is not the case.

Since we cannot control the bandwidth (usually that is a
physical limitation), we want put as many bits per symbol as possible,
without trying to send more data than the maximum capacity (where we
are bound to lose data).  Thus we want to choose a constellation that
is as large as possible, but not too large.

Unfortunately, Shannon's proof of the fact used an exponential-time
algorithm (the run-time wasn't very interesting then), so that still
leaves a question of what encoding would actually achieve the above
rate.  Today, some coding systems can actually come very close (Turbo
codes and others).

Interestingly, Shannon's algorithm was also fundamentally rate-less:
you didn't calculate $r$ up front, but instead simply kept sending
data until it got through.  Rated codes (ones for a fixed $r$) are a
lot easier to design, so we need bit-rate selection, and choosing
constellations, and all of that.  But in the next lecture, we'll look
at rate-less systems, which are nowadays becoming available.

\end{document}

